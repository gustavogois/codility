package lessons.lesson_02;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class Solution_CyclicRotationTest {

    Solution_CyclicRotation solution_cyclicRotation;

    @Before
    public void setUp() throws Exception {
        solution_cyclicRotation = new Solution_CyclicRotation();
    }

    @Test
    public void test1() {
        int[] array = {3, 8, 9, 7, 6};
        int[] answer = {9, 7, 6, 3, 8};
        int k = 3;

        assertTrue(Arrays.equals(answer, solution_cyclicRotation.solution(array, k)));

    }

    @Test
    public void test2() {
        int[] array = {0, 0, 0};
        int[] answer = {0, 0, 0};
        int k = 4;

        assertTrue(Arrays.equals(answer, solution_cyclicRotation.solution(array, k)));

    }
}