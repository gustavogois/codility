package lessons.lesson_01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    Solution solution;

    @Before
    public void initClass() {
        solution = new Solution();
    }

    @Test
    public void when9_should2() {
        // 1001
        assertEquals(2, solution.solution(9));
    }

    @Test
    public void when529_should4() {
        // 1000010001
        assertEquals(4, solution.solution(529));
    }

    @Test
    public void when20_should1() {
        // 10100
        assertEquals(1, solution.solution(20));
    }

    @Test
    public void when15_should0() {
        // 1111
        assertEquals(0, solution.solution(15));
    }

    @Test
    public void when32_should0() {
        // 100000
        assertEquals(0, solution.solution(32));
    }

    @Test
    public void when1041_should5() {
        // 10000010001
        assertEquals(5, solution.solution(1041));
    }

    @Test
    public void verify1() {
        // 1
        assertEquals(0, solution.solution(1));
    }

    @Test
    public void verify2147483647() {
        // 1111111111111111111111111111111
        System.out.println(Integer.toString(2147483647, 2));
        assertEquals(0, solution.solution(2147483647));
    }

    @Test
    public void when1162_should3() {
        // 10010001010
        assertEquals(3, solution.solution(1162));
    }

    @Test
    public void when66561_should9() {
        // 10000010000000001
        assertEquals(9, solution.solution(66561));
    }

    @Test
    public void when328_should2() {
        // 101001000
        assertEquals(2, solution.solution(328));
    }


}