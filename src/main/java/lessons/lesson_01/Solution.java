package lessons.lesson_01;

import java.util.Arrays;

public class Solution {
    public int solution(int n) {
        String binaryRepresentation = Integer.toString(n, 2);
        int firstOnePosition = -1, lengthSequence = 0, lengthestSequence = 0;
        char actualChar;
        for(int i = 0 ; i < binaryRepresentation.length() ; i++) {
            actualChar = binaryRepresentation.charAt(i);
            // Should I start to count? Yes when is 1, the next is 0, and I'm not counting
            if (actualChar == '1') {
                // The next is 0 and I'm not counting
                if (isTheNextEqualZero(binaryRepresentation, i) && firstOnePosition == -1) {

                    firstOnePosition = i;

                        // Should I finish to count? Yes when is 1 and I'm counting
                } else if (firstOnePosition != -1) {
                    lengthSequence = i - firstOnePosition - 1;
                    if (lengthSequence > lengthestSequence) {
                        lengthestSequence = lengthSequence;
                    }
                    // If the next is 0, first will be actual. Otherwise, first will be -1
                    if(isTheNextEqualZero(binaryRepresentation, i)) {
                        firstOnePosition = i;
                    } else {
                        firstOnePosition = -1;
                    }
                }
            }
        }

        return lengthestSequence;
    }

    private boolean isTheNextEqualZero(String binaryRepresentation, int i) {
        return i < binaryRepresentation.length() - 1 && binaryRepresentation.charAt(i + 1) == '0';
    }
}
