package lessons.lesson_02;

public class Solution_CyclicRotation {
    public int[] solution(int[] array, int k) {

        if(array == null) {
            return array;
        }

        int[] solution = new int[array.length];

        for(int i = 0 ; i < array.length ; i++) {
            solution[(i + k) % array.length] = array[i];
        }

        return solution;
    }
}
